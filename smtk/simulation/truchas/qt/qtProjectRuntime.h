//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_truchas_qt_qtProjectRuntime_h
#define smtk_simulation_truchas_qt_qtProjectRuntime_h

#include <QObject>

#include "smtk/simulation/truchas/qt/Exports.h"

#include "smtk/project/Project.h"

/** \brief A singleton class for storing plugin runtime variables.
 *
 * The current plugin design only permits users to keep one smtk project
 * in memory at a time. This class stores a shared pointer to that
 * project instance. A QObject is used for possible inclusion as a
 * paraview "manager" for access by other plugins.
 *
  */
class SMTKTRUCHASQTEXT_EXPORT qtProjectRuntime : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static qtProjectRuntime* instance(QObject* parent = nullptr);
  std::shared_ptr<smtk::project::Project> project() const { return m_project; }
  void setProject(std::shared_ptr<smtk::project::Project> p) { m_project = p; }
  bool unsetProject(std::shared_ptr<smtk::project::Project> p);

protected:
  qtProjectRuntime(QObject* parent = nullptr)
    : Superclass(parent)
  {
  }
  ~qtProjectRuntime() override;

private:
  Q_DISABLE_COPY(qtProjectRuntime);

  std::shared_ptr<smtk::project::Project> m_project;
};

#endif
