//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_simulation_truchas_qt_qtValidators_h
#define __smtk_simulation_truchas_qt_qtValidators_h

// .NAME validators.h - functions for validating material attributes

#include "smtk/simulation/truchas/utility/MaterialAttributeUtils.h"

#include "smtk/PublicPointerDefs.h"

#include "smtk/attribute/Analyses.h"
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/ItemDefinition.h"
#include "smtk/attribute/Resource.h"

#include <QDebug>

#include <limits>
#include <set>
#include <string>
#include <vector>

bool isPhaseElementValid(smtk::attribute::GroupItemPtr phasesItem, std::size_t element);
bool isTransitionElementValid(smtk::attribute::GroupItemPtr transitionsItem, std::size_t element);

inline bool isAttributeValid(
  smtk::attribute::AttributePtr att)
{
  smtk::attribute::ResourcePtr attResource = att->attributeResource();
  auto analysisAtt = attResource->findAttribute("analysis");
  std::set<std::string> categories =
    attResource->analyses().getAnalysisAttributeCategories(analysisAtt);
  std::string reason;
  smtk::simulation::truchas::MaterialAttributeUtils utils;
  bool isValid = utils.isValid(att, categories, reason);

#ifndef NDEBUG
  if (!isValid)
  {
    qWarning() << reason.c_str();
  }
#endif

  return isValid;
}

inline bool isPhaseElementValid(smtk::attribute::GroupItemPtr phasesItem, std::size_t element)
{
  smtk::attribute::ResourcePtr attResource = phasesItem->attribute()->attributeResource();
  auto analysisAtt = attResource->findAttribute("analysis");
  std::set<std::string> categories =
    attResource->analyses().getAnalysisAttributeCategories(analysisAtt);
  std::string reason;
  smtk::simulation::truchas::MaterialAttributeUtils utils;
  bool isValid = utils.isPhaseValid(phasesItem, element, categories, reason);

#ifndef NDEBUG
  if (!isValid)
  {
    qWarning() << reason.c_str();
  }
#endif

  return isValid;
}

inline bool isTransitionElementValid(smtk::attribute::GroupItemPtr transitionsItem,
  std::size_t element)
{
  std::string reason;
  smtk::simulation::truchas::MaterialAttributeUtils utils;

  double minTemp = std::numeric_limits<double>::lowest();
  double maxTemp = minTemp;
  bool isValid = utils.isTransitionValid(transitionsItem, element, minTemp, maxTemp, reason);

#ifndef NDEBUG
  if (!isValid)
  {
    qWarning() << reason.c_str();
  }
#endif

  return isValid;
}

#endif
