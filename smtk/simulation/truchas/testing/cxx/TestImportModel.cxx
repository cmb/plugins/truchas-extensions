//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

// Local incluides
#include "smtk/simulation/truchas/Metadata.h"
#include "smtk/simulation/truchas/Registrar.h"
#include "smtk/simulation/truchas/operations/Create.h"
#include "smtk/simulation/truchas/operations/ImportModel.h"
#include "smtk/simulation/truchas/utility/AttributeUtils.h"
#include "smtk/simulation/truchas/utility/ProjectUtils.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/project/Registrar.h"
#include "smtk/project/operators/Write.h"
#include "smtk/resource/Manager.h"
#include "smtk/session/vtk/Registrar.h"

#include "smtk/common/testing/cxx/helpers.h"

#include <boost/filesystem.hpp>

#include <iostream>

const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);

int TestImportModel(int /*argc*/, char* /*argv*/ [])
{
  // Remove output folder
  boost::filesystem::path scratchPath(SCRATCH_DIR);
  boost::filesystem::path outputPath = scratchPath / "cxx/import_model";
  if (boost::filesystem::is_directory(outputPath))
  {
    boost::filesystem::remove_all(outputPath);
  }

  // Create managers
  smtk::resource::ManagerPtr resourceManager = smtk::resource::Manager::create();
  smtk::attribute::Registrar::registerTo(resourceManager);
  smtk::model::Registrar::registerTo(resourceManager);
  smtk::session::vtk::Registrar::registerTo(resourceManager);

  smtk::operation::ManagerPtr operationManager = smtk::operation::Manager::create();
  smtk::operation::Registrar::registerTo(operationManager);
  smtk::attribute::Registrar::registerTo(operationManager);
  smtk::session::vtk::Registrar::registerTo(operationManager);

  smtk::project::ManagerPtr projectManager =
    smtk::project::Manager::create(resourceManager, operationManager);
  smtk::project::Registrar::registerTo(projectManager);
  smtk::simulation::truchas::Registrar::registerTo(projectManager);

  operationManager->registerResourceManager(resourceManager);

// Create project, copying code from CreateProject test
// Future: use save-as function to copy the CreateProject output
#ifndef WORKFLOWS_SOURCE_DIR
#error WORKFLOWS_SOURCE_DIR must be defined
#endif
  smtk::simulation::truchas::Metadata::WORKFLOWS_DIRECTORY = WORKFLOWS_SOURCE_DIR;

  // Setup and run the create-truchas-project operator
  std::shared_ptr<smtk::project::Project> project;
  {
    auto createOp = operationManager->create<smtk::simulation::truchas::Create>();
    test(createOp != nullptr);

    smtk::attribute::AttributePtr params = createOp->parameters();
    params->findDirectory("location")->setValue(outputPath.string());

    // Data directory is set by CMake (target_compile_definitions)
    boost::filesystem::path dataPath(PROBLEMS_DIR);
    boost::filesystem::path meshPath = dataPath / "meshes/mesh1.gen";
    std::cout << "mesh path: " << meshPath.string() << std::endl;
    test(boost::filesystem::exists(meshPath));
    params->findFile("heat-transfer-mesh")->setIsEnabled(true);
    test(params->findFile("heat-transfer-mesh")->setValue(meshPath.string()));

    params->findFile("induction-heating-mesh")->setIsEnabled(false);

    auto result = createOp->operate();
    int outcome = result->findInt("outcome")->value();
    std::cout << "Create Outcome: " << outcome << std::endl;
    test(outcome == OP_SUCCEEDED);

    smtk::attribute::ResourceItemPtr projectItem = result->findResource("resource");
    auto resource = projectItem->value();
    project = std::dynamic_pointer_cast<smtk::project::Project>(resource);
    test(project != nullptr);
  }

  // Create instanced attributes
  smtk::attribute::ResourcePtr attResource;
  smtk::attribute::GroupItemPtr htItem; // heat transfer analysis item
  {
    // Get the attribute resource
    std::string analysisRole("Analysis1");
    smtk::simulation::truchas::ProjectUtils projectUtils;
    attResource = projectUtils.getByRole<smtk::attribute::Resource>(project, analysisRole);
    test(attResource != nullptr);

    // Set analysis type
    smtk::simulation::truchas::AttributeUtils attUtils;
    auto analysisAtt = attUtils.getAnalysisAtt(attResource);
    test(analysisAtt != nullptr);

    auto analysisItem = analysisAtt->findAs<smtk::attribute::StringItem>(
      "Analysis", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE);
    test(analysisItem != nullptr);
    test(analysisItem->setValue("Truchas"));

    auto item = analysisItem->findChild("Truchas", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE);
    test(item != nullptr);
    auto truchasItem = std::dynamic_pointer_cast<smtk::attribute::GroupItem>(item);
    test(truchasItem != nullptr);

    htItem = truchasItem->findAs<smtk::attribute::GroupItem>(0, "Heat Transfer");
    test(htItem != nullptr);
    htItem->setIsEnabled(true);
  }

  // {
  //   // Write project to file system
  //   std::cout << "Write project to " << project->location() << " ..." << std::endl;
  //   auto writeOp = operationManager->create<smtk::project::Write>();
  //   test(writeOp != nullptr);
  //   test(writeOp->parameters()->associate(project));

  //   auto result = writeOp->operate();
  //   int outcome = result->findInt("outcome")->value();
  //   std::cout << "Write Outcome: " << outcome << std::endl;
  //   if (outcome != OP_SUCCEEDED)
  //   {
  //     std::cout << writeOp->log().convertToString() << std::endl;
  //   }
  //   test(outcome == OP_SUCCEEDED);
  // }

  // Import induction heating model into the project
  {
    auto importOp = operationManager->create<smtk::simulation::truchas::ImportModel>();
    test(importOp->parameters()->associate(project));

    boost::filesystem::path dataPath(PROBLEMS_DIR);
    // Note: mesh4 is not a real EM mesh but is *much* smaller file
    boost::filesystem::path meshPath = dataPath / "meshes/mesh4.gen";
    std::cout << "mesh path: " << meshPath.string() << std::endl;
    test(boost::filesystem::exists(meshPath));

    importOp->parameters()->findFile("induction-heating-mesh")->setIsEnabled(true);
    test(importOp->parameters()->findFile("induction-heating-mesh")->setValue(meshPath.string()));

    auto result = importOp->operate();
    int outcome = result->findInt("outcome")->value();
    std::cout << "Export Outcome: " << outcome << std::endl;
    if (outcome != OP_SUCCEEDED)
    {
      std::cout << importOp->log().convertToString() << std::endl;
    }
    test(outcome == OP_SUCCEEDED);
  }

  // Write project to file system
  {
    std::cout << "Write project to " << project->location() << " ..." << std::endl;
    auto writeOp = operationManager->create<smtk::project::Write>();
    test(writeOp != nullptr);
    test(writeOp->parameters()->associate(project));

    auto result = writeOp->operate();
    int outcome = result->findInt("outcome")->value();
    std::cout << "Write Outcome: " << outcome << std::endl;
    if (outcome != OP_SUCCEEDED)
    {
      std::cout << writeOp->log().convertToString() << std::endl;
    }
    test(outcome == OP_SUCCEEDED);

    // Project should not be marked modified
    test(project->clean());
  }

  return 0;
}
