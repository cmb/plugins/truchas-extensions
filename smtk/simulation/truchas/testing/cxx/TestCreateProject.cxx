//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

// Local includes
#include "smtk/simulation/truchas/Metadata.h"
#include "smtk/simulation/truchas/Registrar.h"
#include "smtk/simulation/truchas/operations/Create.h"
#include "smtk/simulation/truchas/utility/AttributeUtils.h"
#include "smtk/simulation/truchas/utility/ProjectUtils.h"

// SMTK includes
#include "smtk/attribute/Analyses.h"
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Registrar.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Registrar.h"
#include "smtk/project/operators/Write.h"
#include "smtk/resource/Manager.h"
#include "smtk/session/vtk/Registrar.h"
#include "smtk/view/Configuration.h"

#include "smtk/common/testing/cxx/helpers.h"

#include <boost/filesystem.hpp>

#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);

int TestCreateProject(int /*argc*/, char* /*argv*/[])
{
  // Remove output folder
  std::stringstream ss;
  ss << SCRATCH_DIR << "/cxx/"
     << "create_project";
  std::string outputFolder = ss.str();
  if (boost::filesystem::is_directory(outputFolder))
  {
    boost::filesystem::remove_all(outputFolder);
  }

  // Create sundry managers
  smtk::resource::ManagerPtr resourceManager = smtk::resource::Manager::create();
  smtk::attribute::Registrar::registerTo(resourceManager);
  smtk::model::Registrar::registerTo(resourceManager);
  smtk::session::vtk::Registrar::registerTo(resourceManager);

  smtk::operation::ManagerPtr operationManager = smtk::operation::Manager::create();
  smtk::operation::Registrar::registerTo(operationManager);
  smtk::attribute::Registrar::registerTo(operationManager);
  smtk::session::vtk::Registrar::registerTo(operationManager);

  smtk::project::ManagerPtr projectManager =
    smtk::project::Manager::create(resourceManager, operationManager);
  smtk::project::Registrar::registerTo(projectManager);
  smtk::simulation::truchas::Registrar::registerTo(projectManager);

  // Todo Next line is currently a no-op. Is it needed?
  smtk::project::Registrar::registerTo(operationManager);
  operationManager->registerResourceManager(resourceManager);

// Application must set workflows directory
#ifndef WORKFLOWS_SOURCE_DIR
#error WORKFLOWS_SOURCE_DIR must be defined
#endif
  smtk::simulation::truchas::Metadata::WORKFLOWS_DIRECTORY = WORKFLOWS_SOURCE_DIR;

  // Setup and run the create-truchas-project operator
  std::shared_ptr<smtk::project::Project> project;
  smtk::attribute::ResourcePtr attResource;
  smtk::model::ResourcePtr modelResource;
  smtk::simulation::truchas::ProjectUtils projectUtils;
  {
    auto createOp = operationManager->create<smtk::simulation::truchas::Create>();
    test(createOp != nullptr);

    smtk::attribute::AttributePtr params = createOp->parameters();
    params->findDirectory("location")->setValue(outputFolder);

    // Data directory is set by CMake (target_compile_definitions)
    boost::filesystem::path dataPath(PROBLEMS_DIR);
    boost::filesystem::path meshPath = dataPath / "meshes" / "mesh1.gen";
    std::cout << "mesh path: " << meshPath.string() << std::endl;
    test(boost::filesystem::exists(meshPath));
    params->findFile("heat-transfer-mesh")->setIsEnabled(true);
    test(params->findFile("heat-transfer-mesh")->setValue(meshPath.string()));

    params->findFile("induction-heating-mesh")->setIsEnabled(false);

    auto result = createOp->operate();
    int outcome = result->findInt("outcome")->value();
    std::cout << "Create Outcome: " << outcome << std::endl;
    test(outcome == OP_SUCCEEDED);

    smtk::attribute::ResourceItemPtr projectItem = result->findResource("resource");
    auto resource = projectItem->value();
    test(resource != nullptr);
    std::cout << "Created resource type " << resource->typeName() << std::endl;
    project = std::dynamic_pointer_cast<smtk::project::Project>(resource);
    test(project != nullptr);

    // Check that default att resource was created
    std::string analysisRole("Analysis1");
    attResource = projectUtils.getByRole<smtk::attribute::Resource>(project, analysisRole);
    test(attResource != nullptr);
    std::cout << "Project contains attribute resource with role name \"" << analysisRole << "\""
              << std::endl;

    // Check for HT mesh
    std::string htRole("heat-transfer-mesh");
    modelResource = projectUtils.getByRole<smtk::model::Resource>(project, htRole);
    test(modelResource != nullptr);
    std::cout << "Project contains model resource with role name \"" << htRole << "\"" << std::endl;
  }

  // Create instanced attributes
  {
    smtk::simulation::truchas::AttributeUtils attUtils;

    // Set analysis type
    auto analysisAtt = attUtils.getAnalysisAtt(attResource);
    test(analysisAtt != nullptr);

    auto analysisItem = analysisAtt->findAs<smtk::attribute::StringItem>(
      "Analysis", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE);
    test(analysisItem != nullptr);
    test(analysisItem->setValue("Truchas"));

    auto item = analysisItem->findChild("Truchas", smtk::attribute::SearchStyle::IMMEDIATE_ACTIVE);
    test(item != nullptr);
    // std::cout << "item type: " << smtk::attribute::Item::type2String(item->type()) << std::endl;
    auto truchasItem = std::dynamic_pointer_cast<smtk::attribute::GroupItem>(item);
    test(truchasItem != nullptr);

    auto htItem = truchasItem->find(0, "Heat Transfer");
    test(htItem != nullptr);
    htItem->setIsEnabled(true);
  }

  {
    // Write project to file system
    std::cout << "Write project to " << project->location() << " ..." << std::endl;
    auto writeOp = operationManager->create<smtk::project::Write>();
    test(writeOp != nullptr);
    test(writeOp->parameters()->associate(project));

    auto result = writeOp->operate();
    int outcome = result->findInt("outcome")->value();
    std::cout << "Write Outcome: " << outcome << std::endl;
    if (outcome != OP_SUCCEEDED)
    {
      std::cout << writeOp->log().convertToString() << std::endl;
    }
    test(outcome == OP_SUCCEEDED);

    // Resources should not be marked modified
    test(project->clean());
    test(attResource->clean());
    test(modelResource->clean());
  }

  return 0;
}
