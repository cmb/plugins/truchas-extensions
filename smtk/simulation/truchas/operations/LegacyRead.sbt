<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the Project "Read" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="read" Label="Truchas Project - Legacy Read" BaseType="operation">
      <BriefDescription>
        Read 2019-format project from disk.
      </BriefDescription>
      <DetailedDescription>
        &lt;p&gt;Read a project from disk.
        &lt;p&gt;This operator reads a file representing the
        selected project from disk.
      </DetailedDescription>

      <ItemDefinitions>
        <File Name="filename" Label="File Name" NumberOfRequiredValues="1"
          ShouldExist="true"
          FileFilters="Legacy Project Files (.smtkproject)">
        </File>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(read)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::project::Project"/>
          </Accepts>
        </Resource>
        <Void Name="legacy" Optional="true" IsEnabledByDefault="true" />
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
