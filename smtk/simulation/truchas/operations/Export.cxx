//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/truchas/operations/Export.h"

#include "smtk/simulation/truchas/Metadata.h"
#include "smtk/simulation/truchas/utility/AttributeUtils.h"
#include "smtk/simulation/truchas/utility/ProjectUtils.h"
// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/ReferenceItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/SearchStyle.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/operators/ImportPythonOperation.h"
#include "smtk/project/Project.h"

// Build dir includes
#include "smtk/simulation/truchas/operations/Export_xml.h"

#include <boost/filesystem.hpp>

#include <iostream>
#include <string>

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);

// Macro for returning on error
#define errorMacro(msg)                                                                            \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), msg);                                                              \
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);                        \
  } while (0)
} // namespace

namespace smtk
{
namespace simulation
{
namespace truchas
{

smtk::operation::Operation::Result Export::operateInternal()
{
  // std::cout << "Export Truchas Project" << std::endl;
  auto& logger = this->log();

  // Make sure the python operation (Truchas.py) exists.
  if (Metadata::WORKFLOWS_DIRECTORY.empty())
  {
    errorMacro("Internal Error: Metadata::WORKFLOWS_DIRECTORY not defined.");
  }

  boost::filesystem::path workflowDir(Metadata::WORKFLOWS_DIRECTORY);
  boost::filesystem::path pyPath = workflowDir / "Truchas.py";
  if (!boost::filesystem::exists(pyPath))
  {
    errorMacro("Error: expecing python file at " << pyPath.string());
  }

  // Import the python op
  smtk::operation::ImportPythonOperation::Ptr importPythonOp =
    this->manager()->create<smtk::operation::ImportPythonOperation>();
  if (!importPythonOp)
  {
    errorMacro("Unable to create ImportPythonOperation");
  }
  importPythonOp->parameters()->findFile("filename")->setValue(pyPath.string());
  auto loadResult = importPythonOp->operate();
  if (loadResult->findInt("outcome")->value() != OP_SUCCEEDED)
  {
    smtkErrorMacro(logger, "Failed to load export script " << pyPath.string());
    // Copy the outcome
    int loadOutcome = loadResult->findInt("outcome")->value();
    auto thisOutcome = static_cast<smtk::operation::Operation::Outcome>(loadOutcome);
    return this->createResult(thisOutcome);
  }

  // Access the unique name assigned to the python op
  std::string operationName = loadResult->findString("unique_name")->value();

  // Instantiate the python op
  smtk::operation::Operation::Ptr pythonOp = this->manager()->create(operationName);
  if (pythonOp == nullptr)
  {
    errorMacro("Failed to instantate operation from export script");
  }

  // Configure the python op
  auto refItem = this->parameters()->associations();
  auto project = refItem->valueAs<smtk::project::Project>();

  // Get the simulation attributes
  auto res = this->parameters()->findResource("analysis")->value();
  auto attResource = std::dynamic_pointer_cast<smtk::attribute::Resource>(res);
  if (attResource == nullptr)
  {
    errorMacro("Error: no analysis resource");
  }
  bool setAtt = pythonOp->parameters()->findResource("attributes")->setValue(attResource);
  if (!setAtt)
  {
    errorMacro("Failed to set attribute resource item.");
  }

  // Copy the analysis code from the simulation attributes to the python op
  smtk::simulation::truchas::AttributeUtils attUtils;
  auto analysisAtt = attUtils.getAnalysisAtt(attResource);
  if (analysisAtt == nullptr)
  {
    errorMacro("Unable to get analysis attribute.");
  }
  smtk::attribute::ConstStringItemPtr analysisItem = analysisAtt->findString("Analysis");
  if (!analysisItem->isSet())
  {
    smtkErrorMacro(logger, "No analysis is set in the simulation attributes");
    this->manager()->unregisterOperation(operationName);
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }
  auto analysisCode = analysisItem->value();
  pythonOp->parameters()->findString("analysis-code")->setValue(analysisCode);

  // Copy "output" items from project params to truchas params
  std::vector<std::string> itemNames = { "output-file", "output-folder" };
  for (auto itemName : itemNames)
  {
    smtk::attribute::ConstItemPtr sourceItem =
      this->parameters()->find(itemName, smtk::attribute::SearchStyle::RECURSIVE_ACTIVE);
    auto targetItem =
      pythonOp->parameters()->find(itemName, smtk::attribute::SearchStyle::RECURSIVE_ACTIVE);
    if ((sourceItem != nullptr) && (targetItem != nullptr))
    {
      targetItem->assign(sourceItem);
    }
  } // for (itemName)

  // Set the python op model resource
  std::string errMessage;
  smtk::simulation::truchas::ProjectUtils projectUtils;
  smtk::model::ResourcePtr modelResource = projectUtils.getByRole<smtk::model::Resource>(
    project, Metadata::HEAT_TRANSFER_MESH_ROLE, errMessage);
  if (modelResource == nullptr)
  {
    errorMacro(errMessage);
  }
  pythonOp->parameters()->findResource("model")->setValue(modelResource);

  // Get native mesh file locations from the project metadata
  Metadata mdata;
  mdata.getFromResource(project);

  // Heat Transfer Mesh
  bool htMeshEnabled = !mdata.NativeHeatTransferMeshLocation.empty();
  auto meshItem = pythonOp->parameters()->findFile("mesh-file");
  meshItem->setIsEnabled(htMeshEnabled);
  if (htMeshEnabled)
  {
    meshItem->setValue(mdata.NativeHeatTransferMeshLocation);
  }

  // Check for induction heating option
  if (analysisCode == "Truchas")
  {
    auto ihItem =
      analysisItem->findChild("Induction Heating", smtk::attribute::SearchStyle::RECURSIVE_ACTIVE);
    if (ihItem->isEnabled())
    {
      // Assign Induction heating mesh to exporter
      if (mdata.NativeInductionHeatingMeshLocation.empty())
      {
        smtkWarningMacro(logger, "Missing Induction Heating mesh.");
      }
      else
      {
        auto altMeshItem = pythonOp->parameters()->findFile("alt-mesh-file");
        altMeshItem->setIsEnabled(true);
        bool set = altMeshItem->setValue(mdata.NativeInductionHeatingMeshLocation);
        // std::cout << __FILE__ << __LINE__ << " set altMeshItem to "
        //           << mdata.NativeInductionHeatingMeshLocation << "? " << set << std::endl;
      }
    }
  }

  // Run the python op
  smtk::attribute::AttributePtr pythonResult = pythonOp->operate();

  // Copy the log and outcome
  this->log().append(pythonOp->log());  // copy log messages
  int pythonOutcome = pythonResult->findInt("outcome")->value();
  auto thisOutcome = static_cast<smtk::operation::Operation::Outcome>(pythonOutcome);
  if (pythonOutcome != OP_SUCCEEDED)
  {
    smtkErrorMacro(this->log(), "Outcome code " << pythonOutcome);
  }

  // Release the python operation
  this->manager()->unregisterOperation(operationName);
  return this->createResult(thisOutcome);
}

smtk::operation::Operation::Specification Export::createSpecification()
{
  Specification spec = this->smtk::operation::XMLOperation::createSpecification();
  //std::cout << "Export Op:\n" << this->xmlDescription() << std::endl;

  return spec;
}

const char* Export::xmlDescription() const
{
  return Export_xml;
}
}
}
}
