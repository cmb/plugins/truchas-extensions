<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the Truchas Project "Create" Operation -->
<SMTK_AttributeResource Version="4">
  <Definitions>
    <!-- Parameters -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="export-truchas" BaseType="operation" Label="Export to Truchas">
      <BriefDescription>
        Write Truchas input file.
      </BriefDescription>
      <DetailedDescription>
        Using the specified analysis, this operation writes a Truchas or Genre
        input file to the file system.
      </DetailedDescription>

      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       LockType="Read" Extensible="false" OnlyResources="true"
                       AdvanceLevel="1">
        <BriefDescription>The smtk project with the relevant simulation attributes and model(s).</BriefDescription>
        <Accepts>
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Resource Name="analysis" Label="Analysis" LockType="DoNotLock" AdvanceLevel="1">
        <BriefDescription>The simulation attributes to export.</BriefDescription>
          <Accepts>
            <Resource Name="smtk::attribute::Resource" />
          </Accepts>
        </Resource>
        <String Name="analysis-code" Label="Analysis Code">
          <BriefDescription>The analysis code to write, either Truchas (default) or Genre.</BriefDescription>
          <ChildrenDefinitions>
            <Directory Name="output-folder" Label="Output Folder">
            <BriefDescription>The file system directory to write Genre input files.</BriefDescription>
            </Directory>
            <File Name="output-file" Label="Output File"
              FileFilters="Input files (*.inp);;All files (*.*)" Version="0">
              <BriefDescription>The Truchas file to be generated.</BriefDescription>
            </File>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value>Genre</Value>
              <Items>
                <Item>output-folder</Item>
              </Items>
            </Structure>
            <Structure>
              <Value>Truchas</Value>
              <Items>
                <Item>output-file</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>

        <Void Name="test-mode" Label="Test Mode" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <BriefDescription>For internal use</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(export-truchas)" BaseType="result"></AttDef>
  </Definitions>

  <Views>
    <View Type="Operation" Title="Export Truchas File" TopLevel="true" FilterByAdvanceLevel="true" FilterByCategory="false">
      <InstancedAttributes>
        <Att Name="export-truchas" Type="export-truchas"></Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
