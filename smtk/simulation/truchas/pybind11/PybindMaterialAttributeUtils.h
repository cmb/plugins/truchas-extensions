//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_simulation_truchas_MaterialAttributeUtils_h
#define pybind_smtk_simulation_truchas_MaterialAttributeUtils_h

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "smtk/simulation/truchas/utility/MaterialAttributeUtils.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/GroupItem.h"

#include <set>
#include <string>
#include <tuple>


namespace py = pybind11;

py::class_< smtk::simulation::truchas::MaterialAttributeUtils > pybind11_init_smtk_truchas_MaterialAttributeUtils(py::module &m)
{
  py::class_< smtk::simulation::truchas::MaterialAttributeUtils > instance(m, "MaterialAttributeUtils");
  instance
    .def(py::init<>())
    .def("addPhase", &smtk::simulation::truchas::MaterialAttributeUtils::addPhase)
    .def("configureNewAttribute", &smtk::simulation::truchas::MaterialAttributeUtils::configureNewAttribute)
    .def("removePhase", &smtk::simulation::truchas::MaterialAttributeUtils::removePhase)
    .def("enableSharedItem", &smtk::simulation::truchas::MaterialAttributeUtils::enableSharedItem)
    .def("isValid", (bool (smtk::simulation::truchas::MaterialAttributeUtils::*)
      (smtk::attribute::AttributePtr) const) &smtk::simulation::truchas::MaterialAttributeUtils::isValid)
    .def("isValid", (bool (smtk::simulation::truchas::MaterialAttributeUtils::*)
      (smtk::attribute::AttributePtr, const std::set<std::string>&, std::string&) const)
      &smtk::simulation::truchas::MaterialAttributeUtils::isValid)

    // Convenience method. Returns (isValid, reason) as (bool, str)
    .def("isValidReason", [](
      const smtk::simulation::truchas::MaterialAttributeUtils utils,
      smtk::attribute::AttributePtr att,
      const std::set<std::string>& categories)
      {
        std::string reason;
        bool isValid = utils.isValid(att, categories, reason);
        return std::tuple<bool, std::string>(isValid, reason);
      })

    .def("isPhaseValid", &smtk::simulation::truchas::MaterialAttributeUtils::isPhaseValid)
    .def("isTransitionValid", &smtk::simulation::truchas::MaterialAttributeUtils::isTransitionValid)
    ;
  return instance;
}

#endif
