//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_simulation_truchas_ModelUtils_h
#define pybind_smtk_simulation_truchas_ModelUtils_h

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "smtk/simulation/truchas/utility/ModelUtils.h"

#include "smtk/model/Resource.h"
#include "smtk/project/Project.h"


namespace py = pybind11;

py::class_< smtk::simulation::truchas::ModelUtils > pybind11_init_smtk_truchas_ModelUtils(py::module &m)
{
  py::class_< smtk::simulation::truchas::ModelUtils > instance(m, "ModelUtils");
  instance
    .def(py::init<>())
    .def("assignColors", &smtk::simulation::truchas::ModelUtils::assignColors)
    .def("renameModelEntities", &smtk::simulation::truchas::ModelUtils::renameModelEntities)
    .def("tagResources", &smtk::simulation::truchas::ModelUtils::tagResources)
    ;
  return instance;
}

#endif
