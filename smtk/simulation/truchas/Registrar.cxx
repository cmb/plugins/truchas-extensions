//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/truchas/Registrar.h"

#include "smtk/simulation/truchas/Metadata.h"
#include "smtk/simulation/truchas/operations/Create.h"
#include "smtk/simulation/truchas/operations/Export.h"
#include "smtk/simulation/truchas/operations/ImportModel.h"
#include "smtk/simulation/truchas/operations/LegacyRead.h"

#include <cassert>
#include <tuple>

namespace smtk
{
namespace simulation
{
namespace truchas
{
namespace
{
typedef std::tuple<Create, Export, ImportModel, LegacyRead> OperationList;
}

void Registrar::registerTo(const smtk::project::Manager::Ptr& projectManager)
{
  projectManager->registerProject(Metadata::PROJECT_TYPENAME);
  bool registered = projectManager->registerOperations<OperationList>();
  assert(registered);
}

void Registrar::unregisterFrom(const smtk::project::Manager::Ptr& projectManager)
{
  projectManager->unregisterProject(Metadata::PROJECT_TYPENAME);
  projectManager->unregisterOperations<OperationList>();
}
}
}
}
