<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="electromagnetics" Label="Electromagnetics">
      <ItemDefinitions>
        <Component Name="model" Label="Geometry" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::model::Resource" Filter="model"></Resource>
          </Accepts>
        </Component>
        <Double Name="coordinate-scale-factor" Label="Scale Factor" Version="0">
          <DefaultValue>1.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="BC">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Temperature">
          <DefaultValue>73.1</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Group" Title="Reference Test" TopLevel="true" TabPosition="North" FilterByAdvanceLevel="false" FilterByCategory="false">
      <Views>
        <View Title="EMag"/>
        <View Title="BC"></View>
      </Views>
    </View>
    <View Type="Instanced" Title="EMag">
      <InstancedAttributes>
        <Att Name="electromagnetics" Type="electromagnetics"></Att>
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="BC">
      <AttributeTypes>
        <Att Type="BC"></Att>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>
