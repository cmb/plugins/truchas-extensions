# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""
Export operator for Truchas workflows
"""

import imp
import os
import sys

# Make sure we can import all smtk modules used by the operation
try:
    import smtk
    import smtk.attribute
    import smtk.common
    import smtk.io
    import smtk.model
    import smtk.operation
    import smtk.simulation
except ImportError as err:
    print('ERROR importing smtk modules')
    print(str(err))
    print('sys.path:')
    for p in sys.path:
        print('  ', p)
    print()
    raise

# Add the directory containing this file to the python module search list
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
print('source_file', source_file)
sys.path.insert(0, os.path.dirname(source_file))
# Make sure __file__ is set when using modelbuilder
__file__ = source_file
print('loading', os.path.basename(__file__))

import internal
from internal import writer
imp.reload(writer)  # for development


class Export(smtk.operation.Operation):

    def __init__(self):
        smtk.operation.Operation.__init__(self)

    def name(self):
        return "Export Truchas"

    def operateInternal(self):
        try:
            success = ExportCMB(self)
        except Exception as err:
            self.log().addError('Exception => {}'.format(err))
            print('EXCEPTION:', str(err))
            print('LOG:', self.log().convertToString())
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Return with success
        result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        result.find('success').setValue(int(success))
        return result

    def createSpecification(self):
        spec = self.createBaseSpecification()
        # print('spec:', spec)

        # Load export atts
        source_dir = os.path.abspath(os.path.dirname(__file__))
        # print('source_dir:', source_dir)
        sbt_path = os.path.join(source_dir, 'internal', 'truchas-export.sbt')
        print('sbt_path:', sbt_path)
        reader = smtk.io.AttributeReader()
        result = reader.read(spec, sbt_path, self.log())
        # print('reader result:', result)

        # Setup result definition
        resultDef = spec.createDefinition('test result', 'result')
        successDef = smtk.attribute.IntItemDefinition.New('success')
        resultDef.addItemDefinition(successDef)

        return spec

# ---------------------------------------------------------------------


def ExportCMB(export_op):
    """Entry function, called by CMB to write export file."""
    params = export_op.parameters()
    logger = export_op.log()

    # Check version
    version = params.definition().version()
    if version != 2:
        msg = 'Unsupported export parameters version {}, must instead be 2'.format(
            version)
        smtk.ErrorMessage(export_op.log(), msg)
        raise RuntimeError(msg)

    # Get the analysis type
    analysis_item = params.findString('analysis-code')
    analysis_code = analysis_item.value()
    print('Analysis Code:', analysis_code)

    # Get output filename and folder
    output_file_item = analysis_item.find(
        'output-file', smtk.attribute.SearchStyle.RECURSIVE)
    output_file = output_file_item.value(0)
    output_dir = os.path.dirname(output_file)

    # Genre analysis specifies folder separately
    if analysis_code == 'Genre':
        output_dir_item = analysis_item.find(
            'output-folder', smtk.attribute.SearchStyle.RECURSIVE)
        output_dir = output_dir_item.value(0)

    # Create output folder if needed
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Get mesh filename
    mesh_file_item = params.findFile('mesh-file')
    mesh_file = mesh_file_item.value(0)

    altmesh_file = 'Not Specified'
    altmesh_file_item = params.findFile('alt-mesh-file')
    if altmesh_file_item and altmesh_file_item.isEnabled():
        altmesh_file = altmesh_file_item.value(0)

    # Initialize writer
    if analysis_code == 'Genre':
        from internal.writer import genrewriter
        imp.reload(genrewriter)
        writer = genrewriter.GenreWriter(params, mesh_filename=mesh_file)
    elif analysis_code == 'Truchas':
        from internal.writer import truchaswriter
        imp.reload(truchaswriter)
        writer = truchaswriter.TruchasWriter(
            params, mesh_filename=mesh_file, altmesh_filename=altmesh_file)
    else:
        raise RuntimeError(
            'Unrecognized analysis code {}'.format(analysis_code))

    completed = writer.write(output_file)
    print('Writer completion status: %s' % completed)
    if sys.stdout is not None:
        sys.stdout.flush()

    print('Truchas.py number of warnings:', len(writer.warning_messages))
    for msg in writer.warning_messages:
        logger.addWarning(msg)

    return completed
