<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Fluid Flow</Cat>
    <Cat>Void Material</Cat>
  </Categories>
  <Definitions>
    <!-- Boundary Conditions-->
    <AttDef Type="ff.boundary" Label="Boundary Condition" Abstract="true" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="1" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <Categories>
        <Cat>Fluid Flow</Cat>
      </Categories>
    </AttDef>
    <!-- "Inflow" boundary conditions have optional inflow material and temp-->
    <AttDef Type="ff.boundary.inflow" BaseType="ff.boundary" Abstract="true" Version="2">
      <ItemDefinitions>
        <Component Name="material" Label="Material" NumberOfRequiredValues="1" EnforceCategories="true" Optional="true" IsEnableByDefault="false">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='material.real']"></Resource>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='material.void']"></Resource>
          </Accepts>
        </Component>
        <Double Name="inflow_temperature" Label="Inflow Temperature" Optional="true" IsEnabledByDefault="false"></Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ff.boundary.pressure" BaseType="ff.boundary.inflow" Label="Pressure (Dirichlet)" RootName="Pressure">
      <ItemDefinitions>
        <Double Name="pressure" Label="Pressure">
          <ExpressionType>fn.ff.pressure</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ff.boundary.velocity" BaseType="ff.boundary.inflow" Label="Velocity (Dirichlet)" RootName="Velocity">
      <ItemDefinitions>
        <Double Name="velocity" Label="Velocity" NumberOfRequiredValues="3">
          <ExpressionType>fn.ff.velocity</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ff.boundary.no-slip" BaseType="ff.boundary" Label="No Slip" RootName="NoSlip"></AttDef>
    <AttDef Type="ff.boundary.free-slip" BaseType="ff.boundary" Label="Free Slip" RootName="FreeSlip"></AttDef>
    <AttDef Type="ff.boundary.marangoni" BaseType="ff.boundary" Label="Marangoni (tangential surface tension" RootName="Marangoni">
      <ItemDefinitions>
        <Double Name="dsigma" Label="Surface tension coefficient (dsigma)"></Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
