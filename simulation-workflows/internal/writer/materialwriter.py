"""Mixin class for writing materials."""
import smtk
import smtk.common
import smtk.simulation
try:
    import smtksimulationtruchas
except ImportError as err:
    print('ERROR trying to import smtksimulationtruchas module')
    print(str(err))
    import sys
    print('sys.path:')
    for p in sys.path:
        print('  ', p)
    print()
    raise

from .cardformat import CardFormat

INDUCTION_HEATING = 'Induction Heating'

PHASE_FORMAT_LIST = [
    # Note that item_path is actually the item name under the extensible "phases" group item
    CardFormat('specific_heat', item_path='enthalpy/specific-heat',
               expression_keyword='specific_heat_func'),
    CardFormat('specific_enthalpy', item_path='enthalpy/specific-enthalpy',
               expression_keyword='specific_enthalpy_func'),
    CardFormat('conductivity', item_path='conductivity',
               expression_keyword='conductivity_func'),
    CardFormat('density_delta', item_path='fluid/density-delta',
               expression_keyword='density_delta_func'),
    CardFormat('viscosity', item_path='fluid/viscosity'),
    CardFormat('electrical_conductivity', item_path='electrical-conductivity', \
               expression_keyword='electrical_conductivity_func'),
    CardFormat('electric_susceptibility', item_path='electric-susceptibility', \
               expression_keyword='electric_susceptibility_func', skip_default=True),
    CardFormat('magnetic_susceptibility', item_path='magnetic-susceptibility', \
               expression_keyword='magnetic_susceptibility_func', skip_default=True)
]

CHANGE_FORMAT_LIST = [
    [
        # Version 0 formats - relative to the extensible "transitions" group item
        CardFormat('solidus_temp', item_path='lower-transition-temperature'),
        CardFormat('liquidus_temp', item_path='upper-transition-temperature'),
        CardFormat('latent_heat', item_path='latent-heat'),
    ],
    [
        # Version 1 formats - relative to the extensible "transitions" group item
        CardFormat('solidus_temp',
                   item_path='transition-spec/lower-transition-temperature'),
        CardFormat('liquidus_temp',
                   item_path='transition-spec/upper-transition-temperature'),
        CardFormat('solid_frac_table',
                   item_path='transition-spec/solid-fraction-table'),
        CardFormat('latent_heat', item_path='latent-heat'),
    ]
]


class MaterialWriter:
    # ---------------------------------------------------------------------
    def _write_physical_materials(self, namelist, format_list):
        """Writes interleaved MATERIAL, PHASE, MATERIAL_SET namelists."""
        print('Writing MATERIAL/PHASE/MATERIAL_SYSTEM namelists')

        # Write physical materials
        separator = namelist.separator
        att_list = self.sim_atts.findAttributes('material.real')
        att_list.sort(key=lambda att: att.name())
        for att in att_list:
            self._check_material_attribute(att)

            if separator is not None:
                self.out.write('\n')
                label = '### {} '.format(namelist.separator)
                line = label.ljust(80, '#')
                self.out.write(line)
                self.out.write('\n')
                separator = None

            phases_group = att.findGroup('phases')
            num_phases = phases_group.numberOfGroups()

            # For single phase, write MATERIAL namelist (only)
            if num_phases == 1:
                name = phases_group.attribute().name()
                self._write_phase_namelist(
                    phases_group, name, title='MATERIAL')
                continue

            # Create map of <keyword, smtk item> for the shared properties that have
            # been enabled.
            shared_props = self._get_shared_properties(att)

            # For multi-phase, write MATERIAL, PHASE, PHASE_CHANGE
            self._write_mulitphase_material_namelist(
                att, phases_group, shared_props)

            # Write phase and phase_change namelists
            transitions_group = att.findGroup('transitions')
            for element in range(num_phases):
                name = phases_group.find(element, 'name').value()
                self._write_phase_namelist(
                    phases_group, name, element, shared_props)
                if element < num_phases - 1:
                    self._write_change_namelist(
                        transitions_group, phases_group, element)

    def _check_material_attribute(self, att):
        """Checks attribute validity"""
        if att in self.checked_attributes:  # avoid redundancy
            return

        if self.material_att_utils is None:
            self.material_att_utils = smtk.simulation.truchas.MaterialAttributeUtils()

        self.checked_attributes.add(att)
        valid, reason = self.material_att_utils.isValidReason(
            att, self.categories)
        if not valid:
            msg = f'Invalid material attribute \"{att.name()}\": {reason}.'
            print(msg)
            self.warning_messages.append(msg)

    def _get_shared_properties(self, att):
        """Returns dictionary of <keyword, item> for the shared properties that are enabled."""
        shared_dict = dict()
        shared_group = att.findGroup('shared-properties')
        if shared_group is None:
            print('WARNING shared-properties item is missing')
            return shared_dict
        for card in PHASE_FORMAT_LIST:
            if card.item_path.startswith('fluid/'):
                continue  # (fluid properties not shared)
            elif card.item_path.startswith('enthalpy/'):
                item = shared_group.find('enthalpy')
            else:
                item = shared_group.find(card.item_path)
            if item is None:
                print('WARNING shared-propertes item is None:', card.item_path)
                continue
            if item.isEnabled():
                shared_dict[card.keyword] = item
        return shared_dict

    def _write_change_namelist(self, transitions_group, phases_group, element):
        """Writes PHASE_CHANGE namelist for one element in the transitions group item.

        Args:
            transitions_group: smtk.attribute.GroupItem storing transition properties
            phases_group: smtk.attribute.GroupItem storing phase properties
            element: integer indicating which element to write in transitions_group
        """
        self._start_namelist('PHASE_CHANGE')

        low_temp_phase = phases_group.find(element, 'name').value()
        CardFormat.write_value(self.out, 'low_temp_phase', low_temp_phase)
        high_temp_phase = phases_group.find(element + 1, 'name').value()
        CardFormat.write_value(self.out, 'high_temp_phase', high_temp_phase)

        version = transitions_group.definition().version()
        if version == 0:
            for card in CHANGE_FORMAT_LIST[version]:
                card_item = transitions_group.find(element, card.item_path)
                if card_item is not None:
                    card.write_item(card_item, self.out)
        elif version == 1:
            spec_item = transitions_group.find(element, 'transition-spec')
            for card in CHANGE_FORMAT_LIST[version]:
                card_item = None
                if card.item_path.startswith('transition-spec/'):
                    paths = card.item_path.split('/')
                    card_item = spec_item.findChild(
                        paths[-1], smtk.attribute.SearchStyle.ACTIVE_CHILDREN)

                    # Special logic for solid-fraction-table
                    if card_item is not None and paths[-1] == 'solid-fraction-table':
                        expression_att = card_item.expression()
                        data_group = expression_att.find('tabular-data')
                        num_rows = data_group.numberOfGroups()
                        if num_rows < 2:
                            print(
                                'WARNING: number of rows in solid-fraction-table less than 2')
                            continue

                        data = [None] * 2 * num_rows
                        for i in range(num_rows):
                            temp_item = data_group.find(i, 'X')
                            data[2 * i] = temp_item.value()
                            value_item = data_group.find(i, 'Value')
                            data[2 * i + 1] = value_item.value()
                        CardFormat.write_value(
                            self.out, 'solid_frac_table', data)
                        continue
                else:
                    card_item = transitions_group.find(element, card.item_path)

                if card_item is not None:
                    card.write_item(card_item, self.out)
        else:
            RuntimeError('Unexpected version {} for {} item'.format(
                version, transitions_group.name()))

        self._finish_namelist()

    def _write_invariant_other(self, att):
        """Writes phases-invariant properties *except* for density"""
        for name in ['reference-enthalpy', 'reference-temperature']:
            card = CardFormat(
                name, item_path='invariant-properties/{}'.format(name), skip_default=True)
            card.write(self.out, att)

    def _write_mulitphase_material_namelist(self, att, phases_group, shared_props):
        """Writes the MATERIAL namelist for multi-phase materials."""
        self._start_namelist('MATERIAL')

        # Write material name
        CardFormat('name', use_name_for_value=True).write_attribute(
            self.out, att)

        # Write density
        CardFormat(
            'density', item_path='invariant-properties/density').write(self.out, att)

        # Write phases list
        num_phases = phases_group.numberOfGroups()
        phase_list = [None] * num_phases
        for i in range(num_phases):
            name = phases_group.find(i, 'name').value()
            phase_list[i] = '\"{}\"'.format(name[:31])
        CardFormat.write_value(self.out, 'phases', phase_list)

        # Write shared properties
        for card in PHASE_FORMAT_LIST:
            shared_item = shared_props.get(card.keyword)
            if shared_item is not None and shared_item.isEnabled() and shared_item.categories().passes(self.categories):
                card.write_item(shared_item, self.out)

        self._write_invariant_other(att)

        self._finish_namelist()

    def _write_phase_namelist(self, phases_group, name, element=0, shared_props=dict(), title='PHASE'):
        """Writes PHASE namelist for one element in phases group item.

        Args:
            phases_group: smtk.attribute.GroupItem storing phase properties
            name: string representing the phase name
            element: integer indicating which element to write in phases_group
            shared_props: dictionary of <string, smtk.attribute.Item> for
                each property that is shared among all phases
            title: namelist title to use

        The title is overridden as 'MATERIAL' for single-phase materials
        """
        self._start_namelist(title)

        # Write name and is_fluid first
        CardFormat.write_value(self.out, 'name', name[:31])

        fluid_item = phases_group.find(element, 'fluid')
        is_fluid = fluid_item.isEnabled()
        CardFormat.write_value(self.out, 'is_fluid', is_fluid, as_boolean=True)

        att = None
        if title == 'MATERIAL':
            att = phases_group.attribute()
            CardFormat(
                'density', item_path='invariant-properties/density').write(self.out, att)

        enthalpy_item = phases_group.find(
            element, 'enthalpy')  # for special case
        fluid_item = phases_group.find(element, 'fluid')  # ditto
        for card in PHASE_FORMAT_LIST:
            card_item = None
            if card.item_path.startswith('enthalpy/'):
                paths = card.item_path.split('/')
                card_item = enthalpy_item.findChild(
                    paths[-1], smtk.attribute.SearchStyle.ACTIVE_CHILDREN)
            elif card.item_path.startswith('fluid/'):
                paths = card.item_path.split('/')
                card_item = fluid_item.find(paths[-1])
            else:
                card_item = phases_group.find(element, card.item_path)

            if card_item is not None and card_item.isEnabled() and card_item.categories().passes(self.categories):
                card.write_item(card_item, self.out)

        if att is not None:
            self._write_invariant_other(att)

        self._finish_namelist()
